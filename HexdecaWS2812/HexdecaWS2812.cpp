/*  HexdecaWS2812 - High Performance WS2811 LED Display Library
    http://www.pjrc.com/teensy/td_libs_HexdecaWS2812.html
    Copyright (c) 2013 Paul Stoffregen, PJRC.COM, LLC
    Some Teensy-LC support contributed by Mark Baysinger.
    https://forum.pjrc.com/threads/40863-Teensy-LC-port-of-HexdecaWS2812
    Modified to support output only on selected channels by Aleksi Turunen

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <string.h>
#include "HexdecaWS2812.h"


uint16_t HexdecaWS2812::stripLen;
void * HexdecaWS2812::frameBuffer;
void * HexdecaWS2812::drawBuffer;
uint8_t HexdecaWS2812::params;
DMAChannel HexdecaWS2812::dma1;
DMAChannel HexdecaWS2812::dma2;
DMAChannel HexdecaWS2812::dma3;

static uint8_t ones = 0xFF;
static volatile uint8_t update_in_progress = 0;
static uint32_t update_completed_at = 0;

HexdecaWS2812::HexdecaWS2812(uint32_t numPerStrip, void *frameBuf, void *drawBuf, uint8_t config)
{
  stripLen = numPerStrip;
  frameBuffer = frameBuf;
  drawBuffer = drawBuf;
  params = config;
  outputMask = 0xFFFFFFFF;  //Using port clear register
  outMask = 0xFF;    //Source for DMA transfer, used for enabling ports Should be all zeroes, disabled for testing with all ones
}

// Waveform timing: these set the high time for a 0 and 1 bit, as a fraction of
// the total 800 kHz or 400 kHz clock cycle.  The scale is 0 to 255.  The Worldsemi
// datasheet seems T1H should be 600 ns of a 1250 ns cycle, or 48%.  That may
// erroneous information?  Other sources reason the chip actually samples the
// line close to the center of each bit time, so T1H should be 80% if TOH is 20%.
// The chips appear to work based on a simple one-shot delay triggered by the
// rising edge.  At least 1 chip tested retransmits 0 as a 330 ns pulse (26%) and
// a 1 as a 660 ns pulse (53%).  Perhaps it's actually sampling near 500 ns?
// There doesn't seem to be any advantage to making T1H less, as long as there
// is sufficient low time before the end of the cycle, so the next rising edge
// can be detected.  T0H has been lengthened slightly, because the pulse can
// narrow if the DMA controller has extra latency during bus arbitration.  If you
// have an insight about tuning these parameters AND you have actually tested on
// real LED strips, please contact paul@pjrc.com.  Please do not email based only
// on reading the datasheets and purely theoretical analysis.
#define WS2811_TIMING_T0H  60
#define WS2811_TIMING_T1H  176

// Discussion about timing and flicker & color shift issues:
// http://forum.pjrc.com/threads/23877-WS2812B-compatible-with-HexdecaWS2812-library?p=38190&viewfull=1#post38190

void HexdecaWS2812::begin(uint32_t numPerStrip, void *frameBuf, void *drawBuf, uint8_t config)
{
  stripLen = numPerStrip;
  frameBuffer = frameBuf;
  drawBuffer = drawBuf;
  params = config;
  begin();
}

void HexdecaWS2812::begin(void)
{
  uint32_t bufsize, frequency;
  bufsize = stripLen * 48;

  // set up the buffers
  memset(frameBuffer, 0xff, bufsize);  //Buffer logic is inverted due to using pin clear register to write data, logic 1 means "low" bit so off is full ones
  if (drawBuffer) {
    memset(drawBuffer, 0xff, bufsize);
  } else {
    drawBuffer = frameBuffer;
  }



  // output pins configured as outputs in the "enableChannel" method

  // create the two waveforms for WS2811 low and high bits
  switch (params & 0xF0) {
    case WS2811_400kHz:
      frequency = 400000;
      break;
    case WS2811_800kHz:
      frequency = 800000;
      break;
    case WS2813_800kHz:
      frequency = 800000;
      break;
    default:
      frequency = 800000;
  }


#if defined(__MK20DX128__)
  FTM1_SC = 0;
  FTM1_CNT = 0;
  uint32_t mod = (F_BUS + frequency / 2) / frequency;
  FTM1_MOD = mod - 1;
  FTM1_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0);
  FTM1_C0SC = 0x69;
  FTM1_C1SC = 0x69;
  FTM1_C0V = (mod * WS2811_TIMING_T0H) >> 8;
  FTM1_C1V = (mod * WS2811_TIMING_T1H) >> 8;
  // pin 16 triggers DMA(port B) on rising edge
  CORE_PIN16_CONFIG = PORT_PCR_IRQC(1) | PORT_PCR_MUX(3);
  //CORE_PIN4_CONFIG = PORT_PCR_MUX(3); // testing only

#elif defined(__MK20DX256__)
  FTM2_SC = 0;
  FTM2_CNT = 0;
  uint32_t mod = (F_BUS + frequency / 2) / frequency;
  FTM2_MOD = mod - 1;
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0);
  FTM2_C0SC = 0x69;
  FTM2_C1SC = 0x69;
  FTM2_C0V = (mod * WS2811_TIMING_T0H) >> 8;
  FTM2_C1V = (mod * WS2811_TIMING_T1H) >> 8;
  // pin 32 is FTM2_CH0, PTB18, triggers DMA(port B) on rising edge
  // pin 25 is FTM2_CH1, PTB19
  CORE_PIN32_CONFIG = PORT_PCR_IRQC(1) | PORT_PCR_MUX(3);
  //CORE_PIN25_CONFIG = PORT_PCR_MUX(3); // testing only

#elif defined(__MK64FX512__) || defined(__MK66FX1M0__)
  FTM2_SC = 0;
  FTM2_CNT = 0;
  uint32_t mod = (F_BUS + frequency / 2) / frequency;
  FTM2_MOD = mod - 1;
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0);
  FTM2_C0SC = 0x69;
  FTM2_C1SC = 0x69;
  FTM2_C0V = (mod * WS2811_TIMING_T0H) >> 8;
  FTM2_C1V = (mod * WS2811_TIMING_T1H) >> 8;
  // FTM2_CH0, PTA10 (not connected), triggers DMA(port A) on rising edge
  PORTA_PCR10 = PORT_PCR_IRQC(1) | PORT_PCR_MUX(3);

#elif defined(__MKL26Z64__)
  FTM2_SC = 0;
  FTM2_CNT = 0;
  uint32_t mod = F_CPU / f  requency;
  FTM2_MOD = mod - 1;
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0);
  FTM2_C0SC = FTM_CSC_CHF | FTM_CSC_MSB | FTM_CSC_ELSB;
  FTM2_C1SC = FTM_CSC_CHF | FTM_CSC_MSB | FTM_CSC_ELSB;
  TPM2_C0V = mod - ((mod * WS2811_TIMING_T1H) >> 8);
  TPM2_C1V = mod - ((mod * WS2811_TIMING_T1H) >> 8) + ((mod * WS2811_TIMING_T0H) >> 8);

#endif

#define PORT_SPACING 0x40    //Spacing between port C to D
#define MLOFFYES (1+1)
#define FREEZE_DEST_ADDR_BITS 7 //Modulo


  DMA_CR |= (1 << 7);

  // DMA channel #1 sets WS2811 high at the beginning of each cycle
  dma1.source(outMask);
  dma1.destination(GPIOC_PSOR);
  //dma1.transferSize(1);
  //dma1.transferCount(transfercount);
  dma1.disableOnCompletion();
  dma1.TCD->DLASTSGA = 0;
  dma1.TCD->BITER_ELINKNO = bufsize / 2;
  dma1.TCD->CITER_ELINKNO = bufsize / 2;
  dma1.TCD->SOFF = 0;
  dma1.TCD->DOFF = PORT_SPACING;                  //Destination address offset (between ports C and D)
  dma1.TCD->ATTR = DMA_TCD_ATTR_SSIZE(0) | DMA_TCD_ATTR_DSIZE(0) | (FREEZE_DEST_ADDR_BITS << 3); //Modulo for alternating between C and D, jump on every 8:th bit
  dma1.TCD->NBYTES_MLOFFYES = MLOFFYES;

  // DMA channel #2 writes the pixel data at 23% of the cycle
  dma2.sourceBuffer((uint8_t *)frameBuffer, bufsize);
  dma2.destination(GPIOC_PCOR);
  //dma2.transferSize(1);
  //dma2.transferCount(transfercount);
  dma2.disableOnCompletion();
  dma2.TCD->DLASTSGA = 0;
  dma2.TCD->BITER_ELINKNO = bufsize / 2;
  dma2.TCD->CITER_ELINKNO = bufsize / 2;
  dma2.TCD->SOFF = 1;
  dma2.TCD->DOFF = PORT_SPACING;
  dma2.TCD->ATTR = DMA_TCD_ATTR_SSIZE(0) | DMA_TCD_ATTR_DSIZE(0) | (FREEZE_DEST_ADDR_BITS << 3);
  dma2.TCD->NBYTES_MLOFFYES = MLOFFYES;
  dma2.TCD->SLAST = -bufsize;

  // DMA channel #3 clear all the pins low at 69% of the cycle
  dma3.source(outMask);
  dma3.destination(GPIOC_PCOR);
  //dma3.transferSize(1);
  //dma3.transferCount(transfercount);
  dma3.disableOnCompletion();
  dma3.interruptAtCompletion();
  dma3.TCD->DLASTSGA = 0;
  dma3.TCD->BITER_ELINKNO = bufsize / 2;
  dma3.TCD->CITER_ELINKNO = bufsize / 2;
  dma3.TCD->SOFF = 0;
  dma3.TCD->DOFF = PORT_SPACING;
  dma3.TCD->ATTR = DMA_TCD_ATTR_SSIZE(0) | DMA_TCD_ATTR_DSIZE(0) | (FREEZE_DEST_ADDR_BITS << 3);
  dma3.TCD->NBYTES_MLOFFYES = MLOFFYES;




#if defined(__MK20DX128__)
  // route the edge detect interrupts to trigger the 3 channels
  dma1.triggerAtHardwareEvent(DMAMUX_SOURCE_PORTB);
  dma2.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM1_CH0);
  dma3.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM1_CH1);
  DMAPriorityOrder(dma3, dma2, dma1);
#elif defined(__MK20DX256__)
  // route the edge detect interrupts to trigger the 3 channels
  dma1.triggerAtHardwareEvent(DMAMUX_SOURCE_PORTB);
  dma2.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_CH0);
  dma3.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_CH1);
  DMAPriorityOrder(dma3, dma2, dma1);
#elif defined(__MK64FX512__) || defined(__MK66FX1M0__)
  // route the edge detect interrupts to trigger the 3 channels
  dma1.triggerAtHardwareEvent(DMAMUX_SOURCE_PORTA);
  dma2.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_CH0);
  dma3.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_CH1);
  DMAPriorityOrder(dma3, dma2, dma1);
#elif defined(__MKL26Z64__)
  // route the timer interrupts to trigger the 3 channels
  dma1.triggerAtHardwareEvent(DMAMUX_SOURCE_TPM2_CH0);
  dma2.triggerAtHardwareEvent(DMAMUX_SOURCE_TPM2_CH1);
  dma3.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_OV);
#endif

  // enable a done interrupts when channel #3 completes
  dma3.attachInterrupt(isr);
  //pinMode(9, OUTPUT); // testing: oscilloscope trigger
}

void HexdecaWS2812::isr(void)
{
  //digitalWriteFast(9, HIGH);
  //Serial1.print(".");
  //Serial1.println(dma3.CFG->DCR, HEX);
  //Serial1.print(dma3.CFG->DSR_BCR > 24, HEX);
  dma3.clearInterrupt();
  //GPIOC_PCOR = 0xFF;
  //GPIOD_PCOR = 0xFF;
#if defined(__MKL26Z64__)
  GPIOD_PCOR = 0xFF;
#endif
  //Serial1.print("*");
  update_completed_at = micros();
  update_in_progress = 0;
  //digitalWriteFast(9, LOW);
}

int HexdecaWS2812::busy(void)
{
  if (update_in_progress) return 1;
  // busy for 50 (or 300 for ws2813) us after the done interrupt, for WS2811 reset
  if (micros() - update_completed_at < 300) return 1;
  return 0;
}

void HexdecaWS2812::show(void)
{
  // wait for any prior DMA operation
  //Serial1.print("1");
  //while (update_in_progress) ;  //Use this to ensue everything always gets shown
  if (update_in_progress) {       //Use this to prevent delays here, even for a while, good for flight controllers or other time critical stuff
    return;   //Don't do anything if currently busy
  }
  //Serial1.print("2");
  // it's ok to copy the drawing buffer to the frame buffer
  // during the 50us WS2811 reset time
  if (drawBuffer != frameBuffer) {
    // TODO: this could be faster with DMA, especially if the
    // buffers are 32 bit aligned... but does it matter?
    memcpy(frameBuffer, drawBuffer, stripLen * 48);
  }

  // wait for WS2811 reset
  // while (micros() - update_completed_at < 300) ;
  // ok to start, but we must be very careful to begin
  // without any prior 3 x 800kHz DMA requests pending

  //If not ok to start, do nothing to prevent delays
  if (micros() - update_completed_at < 300) {
    return;
  }

#if defined(__MK20DX128__)
  uint32_t cv = FTM1_C0V;
  noInterrupts();
  // CAUTION: this code is timing critical.
  while (FTM1_CNT <= cv) ;
  while (FTM1_CNT > cv) ; // wait for beginning of an 800 kHz cycle
  while (FTM1_CNT < cv) ;
  FTM1_SC = 0;            // stop FTM1 timer (hopefully before it rolls over)
  FTM1_CNT = 0;
  update_in_progress = 1;
  //digitalWriteFast(9, HIGH); // oscilloscope trigger
  PORTB_ISFR = (1 << 0);  // clear any prior rising edge
  uint32_t tmp __attribute__((unused));
  FTM1_C0SC = 0x28;
  tmp = FTM1_C0SC;        // clear any prior timer DMA triggers
  FTM1_C0SC = 0x69;
  FTM1_C1SC = 0x28;
  tmp = FTM1_C1SC;
  FTM1_C1SC = 0x69;
  dma1.enable();
  dma2.enable();          // enable all 3 DMA channels
  dma3.enable();
  FTM1_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0); // restart FTM1 timer
  //digitalWriteFast(9, LOW);

#elif defined(__MK20DX256__)
  FTM2_C0SC = 0x28;
  FTM2_C1SC = 0x28;
  uint32_t cv = FTM2_C0V;
  noInterrupts();
  // CAUTION: this code is timing critical.
  while (FTM2_CNT <= cv) ;
  while (FTM2_CNT > cv) ; // wait for beginning of an 800 kHz cycle
  while (FTM2_CNT < cv) ;
  FTM2_SC = 0;             // stop FTM2 timer (hopefully before it rolls over)
  FTM2_CNT = 0;
  update_in_progress = 1;
  //digitalWriteFast(9, HIGH); // oscilloscope trigger
  PORTB_ISFR = (1 << 18);  // clear any prior rising edge
  uint32_t tmp __attribute__((unused));
  FTM2_C0SC = 0x28;
  tmp = FTM2_C0SC;         // clear any prior timer DMA triggers
  FTM2_C0SC = 0x69;
  FTM2_C1SC = 0x28;
  tmp = FTM2_C1SC;
  FTM2_C1SC = 0x69;
  dma1.enable();
  dma2.enable();           // enable all 3 DMA channels
  dma3.enable();
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0); // restart FTM2 timer
  //digitalWriteFast(9, LOW);

#elif defined(__MK64FX512__) || defined(__MK66FX1M0__)
  FTM2_C0SC = 0x28;
  FTM2_C1SC = 0x28;
  uint32_t cv = FTM2_C1V;
  noInterrupts();
  // CAUTION: this code is timing critical.
  while (FTM2_CNT <= cv) ;
  while (FTM2_CNT > cv) ; // wait for beginning of an 800 kHz cycle
  while (FTM2_CNT < cv) ;
  FTM2_SC = 0;             // stop FTM2 timer (hopefully before it rolls over)
  FTM2_CNT = 0;
  update_in_progress = 1;
  //digitalWriteFast(9, HIGH); // oscilloscope trigger
#if defined(__MK64FX512__)
  asm("nop");
#endif
  PORTA_ISFR = (1 << 10);  // clear any prior rising edge
  uint32_t tmp __attribute__((unused));
  FTM2_C0SC = 0x28;
  tmp = FTM2_C0SC;         // clear any prior timer DMA triggers
  FTM2_C0SC = 0x69;
  FTM2_C1SC = 0x28;
  tmp = FTM2_C1SC;
  FTM2_C1SC = 0x69;
  dma1.enable();
  dma2.enable();           // enable all 3 DMA channels
  dma3.enable();
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0); // restart FTM2 timer
  //digitalWriteFast(9, LOW);

#elif defined(__MKL26Z64__)
  uint32_t sc __attribute__((unused)) = FTM2_SC;
  uint32_t cv = FTM2_C1V;
  noInterrupts();
  while (FTM2_CNT <= cv) ;
  while (FTM2_CNT > cv) ; // wait for beginning of an 800 kHz cycle
  while (FTM2_CNT < cv) ;
  FTM2_SC = 0;		// stop FTM2 timer (hopefully before it rolls over)
  update_in_progress = 1;
  //digitalWriteFast(9, HIGH); // oscilloscope trigger
  dma1.clearComplete();
  dma2.clearComplete();
  dma3.clearComplete();
  uint32_t bufsize = stripLen * 48;
  dma1.transferCount(transfercount);
  dma2.transferCount(transfercount);
  dma3.transferCount(transfercount);
  dma2.sourceBuffer((uint8_t *)frameBuffer, bufsize);
  // clear any pending event flags
  FTM2_SC = FTM_SC_TOF;
  FTM2_C0SC = FTM_CSC_CHF | FTM_CSC_MSB | FTM_CSC_ELSB | FTM_CSC_DMA;
  FTM2_C1SC = FTM_CSC_CHF | FTM_CSC_MSB | FTM_CSC_ELSB | FTM_CSC_DMA;
  // clear any prior pending DMA requests
  dma1.enable();
  dma2.enable();		// enable all 3 DMA channels
  dma3.enable();
  FTM2_CNT = 0; // writing any value resets counter
  FTM2_SC = FTM_SC_DMA | FTM_SC_CLKS(1) | FTM_SC_PS(0);
  //digitalWriteFast(9, LOW);
#endif
  //Serial1.print("3");
  interrupts();
  //Serial1.print("4");
}

void HexdecaWS2812::setPixel(uint32_t num, int color)
{
  if(num>=numPixels()) return;  //Out of bounds, do nothing
  
  uint32_t strip, offset, mask32;
  uint32_t *p;


  //Inpt is always rgb, it is mangled to the proper color order by the below bit

  switch (params & 7) {
    case WS2811_RBG:
      color = (color & 0xFF0000) | ((color << 8) & 0x00FF00) | ((color >> 8) & 0x0000FF);
      break;
    case WS2811_GRB: // <- usually this with ws2812
      color = ((color << 8) & 0xFF0000) | ((color >> 8) & 0x00FF00) | (color & 0x0000FF);
      break;
    case WS2811_GBR:
      color = ((color << 16) & 0xFF0000) | ((color >> 8) & 0x00FFFF);
      break;
    case WS2811_BRG:
      color = ((color << 8) & 0xFFFF00) | ((color >> 16) & 0x0000FF);
      break;
    case WS2811_BGR:
      color = ((color << 16) & 0xFF0000) | (color & 0x00FF00) | ((color >> 16) & 0x0000FF);
      break;
    default:
      break;
  }
  strip = num / stripLen;  // On which ledstrip we area
  offset = num % stripLen; //On which led on the strip we are

  p = ((uint32_t *) drawBuffer) + offset * 12;  //Find right place in buffer. Each led takes 6 bytes, but since upper and lower 8 channels are interleaved, this is 12 

  if (strip < 8) { //If this LED is in the first set of strips, in PORTC, make a mask with that in mind

    //===These are for port C, first 8 channels ======
    //How many times a bit needs to be shifted to end up on the right channel (after it has been spliced to the right position in a pulse)
    //In the first 8 channels it is simply the number of channel. For the last 8 it must be additionally shifter 8 towards msb because of the interleaved channels

    //Flip the low/high logic levels since we're using pin clear register instead of data out register (logic 1 clears the port, logic 0 does nothing)
    //Bytes are sent starting from the least significant bit, so in RGB first G bit (pos 23) is shifted firs in line to position 0 and so on

    mask32 = (0x00010001) << strip; //for blanking all bits corresponding to one strip, for every 4 byte (uint32_t)

    // Set bytes 0-1, these are first 2 bits (0-1) in burst (first int32), green bits 0-1 in grb
    *p |= mask32;
    *p &= ~((((color & 0x800000) >> 23) | ((color & 0x400000) >> 6)) << strip);
    *p &= outputMask;  //Mask 4 bytes of output to avoid signals on unwanted pins

    // Set bytes 2-3
    *++p |= mask32;
    *p &= ~((((color & 0x200000) >> 21) | ((color & 0x100000) >> 4)) << strip);
    *p &= outputMask;

    // Set bytes 4-5
    *++p |= mask32;
    *p &= ~((((color & 0x80000) >> 19) | ((color & 0x40000) >> 2)) << strip);
    *p &= outputMask;

    // Set bytes 6-7
    *++p |= mask32;
    *p &= ~((((color & 0x20000) >> 17) | ((color & 0x10000))) << strip);
    *p &= outputMask;

    // Set bytes 8-9    ---
    *++p |= mask32;
    *p &= ~((((color & 0x8000) >> 15) | ((color & 0x4000) << 2)) << strip);
    *p &= outputMask;

    // Set bytes 10-11
    *++p |= mask32;
    *p &= ~((((color & 0x2000) >> 13) | ((color & 0x1000) << 4)) << strip);
    *p &= outputMask;

    // Set bytes 12-13
    *++p |= mask32;
    *p &= ~((((color & 0x800) >> 11) | ((color & 0x400) << 6)) << strip);
    *p &= outputMask;

    // Set bytes 14-15 .......
    *++p |= mask32;
    *p &= ~((((color & 0x200) >> 9) | ((color & 0x100) << 8)) << strip);
    *p &= outputMask;

    // Set bytes 16-17
    *++p |= mask32;
    *p &= ~((((color & 0x80) >> 7) | ((color & 0x40) << 10)) << strip);
    *p &= outputMask;

    // Set bytes 18-19
    *++p |= mask32;
    *p &= ~((((color & 0x20) >> 5) | ((color & 0x10) << 12)) << strip);
    *p &= outputMask;

    // Set bytes 20-21
    *++p |= mask32;
    *p &= ~((((color & 0x8) >> 3) | ((color & 0x4) << 14)) << strip);
    *p &= outputMask;

    // Set bytes 22-23
    *++p |= mask32;
    *p &= ~((((color & 0x2) >> 1) | ((color & 0x1) << 16)) << strip);
    *p &= outputMask;

  }
  else {   //Else it is in the second set of strips, in PORTD

    mask32 = (0x01000100) << (strip-8); //for blanking all bits corresponding to one strip

    //===These are for port D, last 8 channels ======

    // Set bytes 0-1, these are first 2 bits (0-1) in burst (first int32), green bits 0-1 in grb
    *p |= mask32;
    *p &= ~((((color & 0x800000) >> 15) | ((color & 0x400000) << 2)) << (strip-8));
    *p &= outputMask;  //Mask 4 bytes of output to avoid signals on unwanted pins

    // Set bytes 2-3
    *++p |= mask32;
    *p &= ~((((color & 0x200000) >> 13) | ((color & 0x100000) << 4)) << (strip-8));
    *p &= outputMask;

    // Set bytes 4-5
    *++p |= mask32;
    *p &= ~((((color & 0x80000) >> 11) | ((color & 0x40000) << 6)) << (strip-8));
    *p &= outputMask;

    // Set bytes 6-7
    *++p |= mask32;
    *p &= ~((((color & 0x20000) >> 9) | ((color & 0x10000) << 8)) << (strip-8));
    *p &= outputMask;

    // Set bytes 8-9
    *++p |= mask32;
    *p &= ~((((color & 0x8000) >> 7) | ((color & 0x4000) << 10)) << (strip-8));
    *p &= outputMask;

    // Set bytes 10-11
    *++p |= mask32;
    *p &= ~((((color & 0x2000) >> 5) | ((color & 0x1000) << 12)) << (strip-8));
    *p &= outputMask;

    // Set bytes 12-13
    *++p |= mask32;
    *p &= ~((((color & 0x800) >> 3) | ((color & 0x400) << 14)) << (strip-8));
    *p &= outputMask;

    // Set bytes 14-15
    *++p |= mask32;
    *p &= ~((((color & 0x200) >> 1) | ((color & 0x100) << 16)) << (strip-8));
    *p &= outputMask;

    // Set bytes 16-17
    *++p |= mask32;
    *p &= ~((((color & 0x80) << 1) | ((color & 0x40) << 18)) << (strip-8));
    *p &= outputMask;

    // Set bytes 18-19
    *++p |= mask32;
    *p &= ~((((color & 0x20) << 3) | ((color & 0x10) << 20)) << (strip-8));
    *p &= outputMask;

    // Set bytes 20-21
    *++p |= mask32;
    *p &= ~((((color & 0x8) << 5) | ((color & 0x4) << 22)) << (strip-8));
    *p &= outputMask;

    // Set bytes 22-23
    *++p |= mask32;
    *p &= ~((((color & 0x2) << 7) | ((color & 0x1) << 24)) << (strip-8));
    *p &= outputMask;
  }
}

int HexdecaWS2812::getPixel(uint32_t num)
{
  uint32_t strip, offset, mask;
  uint8_t bit, *p;
  int color = 0;

  strip = num / stripLen;
  offset = num % stripLen;
  bit = (1 << strip);
  p = ((uint8_t *)drawBuffer) + offset * 24;
  for (mask = (1 << 23) ; mask ; mask >>= 1) {
    if (*p++ & bit) color |= mask;
  }
  switch (params & 7) {
    case WS2811_RBG:
      color = (color & 0xFF0000) | ((color << 8) & 0x00FF00) | ((color >> 8) & 0x0000FF);
      break;
    case WS2811_GRB:
      color = ((color << 8) & 0xFF0000) | ((color >> 8) & 0x00FF00) | (color & 0x0000FF);
      break;
    case WS2811_GBR:
      color = ((color << 8) & 0xFFFF00) | ((color >> 16) & 0x0000FF);
      break;
    case WS2811_BRG:
      color = ((color << 16) & 0xFF0000) | ((color >> 8) & 0x00FFFF);
      break;
    case WS2811_BGR:
      color = ((color << 16) & 0xFF0000) | (color & 0x00FF00) | ((color >> 16) & 0x0000FF);
      break;
    default:
      break;
  }
  return color;
}

void HexdecaWS2812::enableChannel(uint8_t num) {
  if (num < 16) {       //if channel is in range, add correct output masking bits
    while (update_in_progress) {} //Don't modify these if leds are being updates.
    //outMask |= (1 << num);
    //outputMask |= (0x01010101) << num;
    //Set corresponding pin as output
    switch (num) {
      case 0:
        pinMode(15, OUTPUT);  // pin 2 strip #1 port C0
        break;
      case 1:
        pinMode(22, OUTPUT);  // pin 14 strip #2 port C1
        break;
      case 2:
        pinMode(23, OUTPUT);  // pin 7 strip #3 port C2
        break;
      case 3:
        pinMode(9, OUTPUT);  // pin 8 strip #4 port C3
        break;
      case 4:
        pinMode(10, OUTPUT);  // pin 6 strip #5 port C4
        break;
      case 5:
        pinMode(13, OUTPUT);  //pin 20 strip #6 port C5
        break;
      case 6:
        pinMode(11, OUTPUT);  // pin 21 strip #7 port C6
        break;
      case 7:
        pinMode(12, OUTPUT);  // pin 5 strip #8 port C7
        break;
      case 8:
        pinMode(2, OUTPUT);  // pin 2 strip #9 port D0
        break;
      case 9:
        pinMode(14, OUTPUT);  // pin 14 strip #10 port D1
        break;
      case 10:
        pinMode(7, OUTPUT);  // pin 7 strip #11 port D2
        break;
      case 11:
        pinMode(8, OUTPUT);  // pin 8 strip #12 port D3
        break;
      case 12:
        pinMode(6, OUTPUT);  // pin 6 strip #13 port D4
        break;
      case 13:
        pinMode(20, OUTPUT);  //pin 20 strip #14 port D5
        break;
      case 14:
        pinMode(21, OUTPUT);  // pin 21 strip #15 port D6
        break;
      case 15:
        pinMode(5, OUTPUT);  // pin 5 strip #16 port D7
        break;
      default:
        break;
    }
  }
}

void HexdecaWS2812::fill_solid(int32_t rangestart, int32_t amount, int32_t col){
  //if(rangestart < 0 || (rangestart + amount) > numPixels() || rangestart > rangestart+amount) return;  //Check for out of range situations
  //SetPixel takes care of out of range situations

  for(int i = 0; i < amount; i++){
    setPixel(rangestart+i,col);
  }
}
