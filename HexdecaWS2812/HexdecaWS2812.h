/*  HexdecaWS2812 - High Performance WS2811 LED Display Library
    http://www.pjrc.com/teensy/td_libs_HexdecaWS2812.html
    Copyright (c) 2013 Paul Stoffregen, PJRC.COM, LLC

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef HexdecaWS2812_h
#define HexdecaWS2812_h

#ifdef __AVR__
#error "Sorry, HexdecaWS2812 only works on 32 bit Teensy boards.  AVR isn't supported."
#endif

#include <Arduino.h>
#include "DMAChannel.h"

#if TEENSYDUINO < 121
#error "Teensyduino version 1.21 or later is required to compile this library."
#endif
#ifdef __AVR__
#error "HexdecaWS2812 does not work with Teensy 2.0 or Teensy++ 2.0."
#endif

#define WS2811_RGB	0	// The WS2811 datasheet documents this way
#define WS2811_RBG	1
#define WS2811_GRB	2	// Most LED strips are wired this way
#define WS2811_GBR	3
#define WS2811_BRG	4
#define WS2811_BGR	5

#define WS2811_800kHz 0x00	// Nearly all WS2811 are 800 kHz
#define WS2811_400kHz 0x10	// Adafruit's Flora Pixels
#define WS2813_800kHz 0x20	// WS2813 are close to 800 kHz but has 300 us frame set delay


class HexdecaWS2812 {
public:
	HexdecaWS2812(uint32_t numPerStrip, void *frameBuf, void *drawBuf, uint8_t config = WS2811_GRB);
	void begin(void);
	void begin(uint32_t numPerStrip, void *frameBuf, void *drawBuf, uint8_t config = WS2811_GRB);

	void setPixel(uint32_t num, int color);
	void setPixel(uint32_t num, uint8_t red, uint8_t green, uint8_t blue) {
		setPixel(num, color(red, green, blue));
	}
	int getPixel(uint32_t num);

  void enableChannel(uint8_t num);  //Enables a channel from 0-7. By default none are enabled so this must be used to get any output.

                 // pin 15 -> channel 0 strip #1
                // pin 22 -> channel 1 strip #2
               // pin 23 -> channel 2 strip #3
              // pin 9 -> channel 3 strip #4
             // pin 10 -> channel 4 strip #5
            // pin 13 -> channel 5 strip #6
           // pin 11 -> channel 6 strip #7
          // pin 12 -> channel 7 strip #8
          
         // pin 2 -> channel 8 strip #9
        // pin 14 -> channel 9 strip #10
       // pin 7 -> channel 10 strip #11
      // pin 8 -> channel 11 strip #12
     // pin 6 -> channel 12 strip #13
    // pin 20 -> channel 13 strip #14
   // pin 21 -> channel 14 strip #15
  // pin 5 -> channel 15 strip #16

	void show(void);
	int busy(void);

	int numPixels(void) {
		return stripLen * 16;
	}
	int color(uint8_t red, uint8_t green, uint8_t blue) {
		return (red << 16) | (green << 8) | blue;
	}

  void fill_solid(int32_t rangestart, int32_t amount, int32_t col);  //Fills a segment of leds with a color

private:
	static uint16_t stripLen;
	static void *frameBuffer;
	static void *drawBuffer;
	static uint8_t params;
	static DMAChannel dma1, dma2, dma3;
	static void isr(void);
  uint8_t outMask;  //This is used as a source for pulse start and end DMA's replacing fixed "ones"
  uint32_t outputMask;  //This is used to mask the buffer 4 bytes at a time, it is 4 copies of outMask pre-calculated for tiny performance boost

  
  
};

#endif
