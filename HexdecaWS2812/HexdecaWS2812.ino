/*

  Required Connections
  --------------------
    pin 15: LED Strip #1    HexdecaWS2812 drives 16 LED Strips.
    pin 22: LED strip #2    All 16 are the same length.
    pin 23: LED strip #3
    pin 9:  LED strip #4    A 100 ohm resistor should used
    pin 10: LED strip #5    between each Teensy pin and the
    pin 13: LED strip #6    wire to the LED strip, to minimize
    pin 11: LED strip #7    high frequency ringining & noise.
    pin 12: LED strip #8
    pin 2:  LED Strip #9
    pin 14: LED strip #10
    pin 7:  LED strip #11
    pin 8:  LED strip #12 
    pin 6:  LED strip #13
    pin 20: LED strip #14
    pin 21: LED strip #15
    pin 5:  LED strip #16
    pin 4 - Do not use
    pin 3 - Do not use as PWM.  Normal use is ok.

  This test is useful for checking if your LED strips work, and which
  color config (WS2811_RGB, WS2811_GRB, etc) they require.
*/

#include "HexdecaWS2812.h"

const int ledsPerStrip = 8;

DMAMEM unsigned int displayMemory[ledsPerStrip * 12];
unsigned int drawingMemory[ledsPerStrip * 12];

const int config = WS2811_GRB | WS2811_800kHz;

HexdecaWS2812 leds(ledsPerStrip, displayMemory, drawingMemory, config);

void setup() {
  //pinMode(15,OUTPUT);
  Serial.begin(115200);
  delay(100);
  //Serial.println("Ahoy!");
  leds.enableChannel(0);
  leds.enableChannel(1);
  leds.enableChannel(2);
  leds.enableChannel(3);
  leds.enableChannel(4);
  leds.enableChannel(5);
  leds.enableChannel(6);
  leds.enableChannel(7);
  leds.enableChannel(8);
  leds.enableChannel(9);
  leds.enableChannel(10);
  leds.enableChannel(11);
  leds.enableChannel(12);
  leds.enableChannel(13);
  leds.enableChannel(14);
  leds.enableChannel(15);
  leds.begin();
  
  delay(50);


  //leds.show();
}

#define RED    0x110000
#define GREEN  0x001100
#define BLUE   0x000016
#define YELLOW 0x101400
#define PINK   0x120009
#define ORANGE 0x100400
#define WHITE  0x101010


void loop() {
  //int microsec = 1000000 / leds.numPixels();  // change them all in 2 seconds
  int microsec = 50000;
  static int pos = 0;

  // uncomment for voltage controlled speed
  // millisec = analogRead(A9) / 40;
  //
  //  colorWipe(RED, microsec);
  //  colorWipe(GREEN, microsec);
  //  colorWipe(BLUE, microsec);
  //  colorWipe(YELLOW, microsec);
  //  colorWipe(PINK, microsec);
  //  colorWipe(ORANGE, microsec);
  //  colorWipe(WHITE, microsec);

  //setAllLeds(0x0f0000, microsec);
  setAllLeds(0x000000);
  //leds.setPixel(pos,leds.color(255,0,0));
  leds.fill_solid(pos,10,leds.color(10,10,0));
  leds.show();
  delay(15);

  pos++;
  if(pos >= leds.numPixels()){
    pos = -10;
  }
}
void colorWipe2(int color, int wait)
{
  for (int i = 0; i < 8; i++) {
    leds.setPixel(i, color);
    leds.show();
    delayMicroseconds(100000);
  }
}

void colorWipe(int color, int wait)
{
  for (int i = 0; i < leds.numPixels(); i++) {
    //leds.setPixel(i, color);
    leds.show();
    //    leds2.setPixel(i, color);
    //    leds2.show();
    delayMicroseconds(wait);
  }
}
//leds.numPixels()
void setAllLeds(int color)
{
  for (int i = 0; i < leds.numPixels(); i++) {
    leds.setPixel(i, color);
  }
}
